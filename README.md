Dr James and Kimberly Dawes at Stones River Chiropractic in Murfreesboro TN. Helping people of all ages, including babies. From pain symptoms to postural correction, they are here to make your life better. We welcome potential patients with a complimentary consult to see if our office is a good fit.

Address : 206 N Thompson Lane, Suite B, Murfreesboro, TN 37129

Phone : 615-867-6700